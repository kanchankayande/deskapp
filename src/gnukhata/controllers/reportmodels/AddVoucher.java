/**
 * 
 */
package gnukhata.controllers.reportmodels;

/**
 * @author kk
 *
 */
public class AddVoucher {
private String DrCr;
private String AccountName;
private double DrAmount;
private double CrAmount;
/**
 *@param drCr
 * @param accountName
 * @param drAmount
 * @param crAmount
 */
public AddVoucher(String drCr, String accountName, double drAmount,double crAmount) {
	super();
	DrCr = drCr;
	AccountName = accountName;
	DrAmount = drAmount;
	CrAmount = crAmount;
}
/**
 * @return the drCr
 */
public String getDrCr() {
	return DrCr;
}
/**
 * @return the accountName
 */
public String getAccountName() {
	return AccountName;
}
/**
 * @return the drAmount
 */
public double getDrAmount() {
	return DrAmount;
}
/**
 * @return the crAmount
 */
public double getCrAmount() {
	return CrAmount;
}


}
